<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function() {
            Schema::create('transaction_types', function (Blueprint $table) {
                $table->increments('id');

                $table->string('type');
            });

            Schema::table('transactions', function(Blueprint $table) {
                $table->integer('transaction_type_id')->unsigned()->nullable();
                $table->foreign('transaction_type_id')->references('id')->on('transaction_types');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function() {
            Schema::table('transactions', function(Blueprint $table) {
                $table->dropForeign(['transaction_type_id']);
            });

            Schema::drop('transaction_types');
        });
    }
}
