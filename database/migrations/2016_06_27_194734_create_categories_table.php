<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function() {
            Schema::create('categories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            });

            $id = DB::table('categories')->insertGetId([
                "name" => "Uncategorized"
            ]);

            Schema::table('transactions', function(Blueprint $table) use($id){
                $table->integer('category_id')->unsigned()->default($id);
                $table->foreign('category_id')->references('id')->on('categories');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function() {
            Schema::table('transactions', function(Blueprint $table) {
                $table->dropForeign(['category_id']);
            });

            Schema::drop('categories');
        });
    }
}
