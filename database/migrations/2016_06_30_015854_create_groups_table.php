<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::transaction(function() {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });

        $default = DB::table('groups')->insertGetId([
          'name' => 'Default Group'
        ]);

        Schema::table('users', function(Blueprint $table) use($default){
          $table->integer('group_id')->unsigned()->default($default);
          $table->foreign('group_id')->references('id')->on('groups');
        });
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::transaction(function() {
        Schema::table('users', function($table) {
          $table->dropForeign(['group_id']);
        });
        
        Schema::drop('groups');
      });
    }
}
