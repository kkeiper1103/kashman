<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $fillable = [
      'name', 'email_address', 'phone_number', 'type',
      'conditions', 'message'
    ];

    /**
     *
     */
    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function shoundSend()
    {
      return false;
    }

    /**
     *
     */
    public function send()
    {

    }
}
