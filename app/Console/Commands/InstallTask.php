<?php

namespace App\Console\Commands;

use App\User;
use Hash;
use Illuminate\Console\Command;

class InstallTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Kashman Application';

    /**
     * @var array|mixed
     *
     * Array of all the env variables
     */
    protected $env = [];


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->loadEnv();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting the Installation...');

        $this->checkInstalled();


        //
        $this->configureDatabase();

        //
        $this->configureLocale();

        //
        $this->configureMailSettings();

        //
        if( strcmp($this->env['APP_KEY'], "SomeRandomString") == 0 ) {
            $this->saveEnv();
            $this->call('key:generate');
            $this->loadEnv();
        }

        //
        $this->configureEnvironment();

        //
        $this->configureMiscSettings();

        //
        $this->saveEnv();

        // handle migrations
        $this->handleMigrationsAndSeeds();

        // install admin user
        $this->configureAdminUser();

        // create installed lock file
        touch( base_path('.install') );

        $this->info('Finished Installing Application!');
    }

    private function checkInstalled()
    {
      if( file_exists(base_path('.install')) ) {
        $this->info('Application Has Already Been Installed.');

        if( $this->confirm("Proceed Anyway?", false) == false ) {
          exit( $this->info("Exiting Installation.") );
        }
      }
    }

    /**
     * @param $section_title
     * @param $cb
     */
    private function section($section_title, $cb)
    {
        $this->info(str_repeat('=', strlen($section_title)));
        $this->info($section_title);
        $this->info(str_repeat('=', strlen($section_title)) . "\n");

        if( $this->confirm("Configure {$section_title}?", true) )
            call_user_func($cb);

        $this->info(str_repeat('=', strlen($section_title)) . "\n");
    }

    /**
     *
     */
    private function configureDatabase()
    {
        $this->section('Installing Database...', function() {
            $this->env['DB_CONNECTION'] = $this->ask("Default Connection", 'mysql');


            if(strcasecmp($this->env['DB_CONNECTION'], "sqlite") == 0) {
                touch( database_path('database.sqlite') );

                unset(
                    $this->env['DB_DATABASE'],
                    $this->env['DB_HOST'],
                    $this->env['DB_USERNAME'],
                    $this->env['DB_PASSWORD']
                );
            }
            else {
                $this->env['DB_DATABASE'] = $this->ask('Database', basename(base_path()) . '_' . $this->env['APP_ENV'] );
                $this->env['DB_HOST'] = $this->ask('Database Host', 'localhost');
                $this->env['DB_USERNAME'] = $this->ask('Username', 'root');
                $this->env['DB_PASSWORD'] = $this->secret('Password', 'password');
            }
        });
    }

    /**
     *
     */
    private function loadEnv()
    {
        if( !file_exists(base_path('.env')) ) {
            copy(base_path('.env.example'), base_path('.env'));
        }

        $whole_env = file_get_contents( base_path('.env') );

        $lines = explode("\n", $whole_env);

        array_map(function($value) {
            if(str_contains($value, '=')) {
                list($k, $v) = explode("=", $value);

                $this->env[$k] = $v;
            }
        }, array_filter($lines));

        return $this;
    }

    /**
     *
     */
    private function saveEnv()
    {
        $section = '';

        $lines = array_map(function($v, $k) use(&$section){
            list($tmp) = explode('_', $k);

            $value = '';

            // this inserts a new line when the section changes.
            // eg, MAIL_ to CURRENCY_
            if( $tmp != $section && !empty($section) ) {
                $value .= PHP_EOL;
            }

            $section = $tmp;

            $value .= "{$k}={$v}";

            return $value;
        }, $this->env, array_keys($this->env));

        return file_put_contents(base_path('.env'), implode("\n", $lines));
    }

    /**
     *
     */
    private function configureLocale()
    {
        $this->section("Locale", function() {
            $this->env['CURRENCY_FORMAT'] = $this->ask("What is Your Locale?", 'en_US');
            $this->env['CURRENCY_MARK'] = $this->ask("What Is Your Default Currency?", "$");
        });
    }

    /**
     *
     */
    private function configureEnvironment()
    {
        $this->section('Application Environment', function() {
            $env = $this->ask('What Environment Are You Installing?', 'local');

            $this->env['APP_ENV'] = $env;
        });
    }

    /**
     *
     */
    private function handleMigrationsAndSeeds()
    {
        $this->section('Migrate Database', function() {

            /**
             *
             */
            if( $this->confirm("Should I Migrate the Database?", false) ) {
                try {
                    $this->call('migrate');

                    $this->info('Successfully Migrated Database Tables.');
                } catch (\PDOException $e) {
                    $this->error('Unable to Migrate the Database. Please Do this Manually');

                    $this->error($e->getMessage());
                }
            }

            /**
             *
             */
            if( $this->confirm('Should I Seed the Database?', false) ) {
                try {
                    $this->call('db:seed');

                    $this->info('Database Successfully Seeded.');
                }
                catch(\Exception $e) {
                    $this->error('Unable to Seed Database. Please Do this Manually');

                    $this->error($e->getMessage());
                }
            }
        });
    }

    /**
     *
     */
    private function configureMiscSettings()
    {
        $this->section('Miscellaneous Settings', function() {
            $this->env['CACHE_DRIVER'] = $this->choice("Cache Driver:", [
                'file',
                'database',
                'apc',
                'array',
                'redis',
                'memcached'
            ], 0);


            $this->env['SESSION_DRIVER'] = $this->choice("Session Driver:", [
                "file", "cookie", "database", "apc",
                "memcached", "redis", "array"
            ], 1);


            $this->env['QUEUE_DRIVER'] = $this->choice("Queue Driver:", [
                "null", "sync", "database",
                "beanstalkd", "sqs", "redis"
            ], 1);
        });
    }

    /**
     *
     */
    private function configureAdminUser()
    {
        $this->section('Configure Admin Account', function() {

            $data = [];

            do {
                if(!empty($data)) {
                    $this->error('Invalid Information Entered. Please Enter New Information');
                }

                $data['name'] = $this->ask('Username');
                $data['email'] = $this->ask('Email Address');
                $data['password'] = $this->secret('Password');
                $data['password_confirmation'] = $this->secret("Password Confirmation");

                $validator = \Validator::make($data, [
                    'name' => 'required',
                    'email' => 'required|email',
                    'password' => 'required|confirmed|min:8'
                ]);
            }
            while( $validator->fails() );


            $user = new User($data);

            $user->password = Hash::make($data['password']);

            if( $user->save() ) {
                $this->info('Successfully Added Admin Account!');
            }
            else {
                $this->error('Could Not Create Admin Account.');
            }
        });
    }

    /**
     *
     */
    private function configureMailSettings()
    {
        $this->section('Mail Settings', function() {
            /**
             * MAIL_DRIVER=smtp
            MAIL_HOST=mailtrap.io
            MAIL_PORT=2525
            MAIL_USERNAME=null
            MAIL_PASSWORD=null
            MAIL_ENCRYPTION=null
             */

            $this->env['MAIL_DRIVER'] = $this->choice("Driver", [
                "smtp", "mail", "sendmail",
                "mailgun", "mandrill", "ses",
                "sparkpost", "log"
            ], 0);

            $this->env['MAIL_HOST'] = $this->ask('Host', 'mailtrap.io');
            $this->env['MAIL_PORT'] = $this->ask('Port', 2525);
            $this->env['MAIL_USERNAME'] = $this->ask('Username', 'null');
            $this->env['MAIL_PASSWORD'] = $this->ask('Password', 'null');
            $this->env['MAIL_ENCRYPTION'] = $this->choice('Encryption Method', [
                'tls'
            ], 0);
        });
    }
}
