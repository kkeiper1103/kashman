<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendUserAlerts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alerts:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Alerts Configured By Users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = \App\User::with('alerts')->get();

        $users->each(function($u) {
          $u->alerts->each(function($a) {

            if( $a->shouldSend() )
              $this->queue( [$a, 'send'] );

              
          });
        });
    }
}
