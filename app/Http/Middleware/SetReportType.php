<?php

namespace App\Http\Middleware;

use Closure;
use Route;

class SetReportType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $type = Route::current()->getParameter('type', 'weekly');

        // valid paramters for the type variable
        if( !in_array($type, ['hourly', 'daily', 'weekly', 'monthly', 'yearly']) )
            abort(404);

        $request->headers->set('X-REPORT-TYPE', $type);

        return $next($request);
    }
}
