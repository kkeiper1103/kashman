<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class GroupAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::user()->admin == false )
        {
            return redirect('/')->with('danger', "You Don't Have Permission to Access This Page.");
        }

        return $next($request);
    }
}
