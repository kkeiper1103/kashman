<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


/**
 *
 */
Route::group(['middleware' => 'auth'], function() {

    Route::get('/', function() {
        return view('dashboard');
    });

    Route::get("transactions/json", "TransactionController@getJson");
    Route::resource('transactions', TransactionController::class, [
        'except' => [
            'create', 'edit'
        ]
    ]);


    Route::get('reports', 'ReportController@index');
    Route::get('reports/data', 'ReportController@data');


    Route::resource('categories', CategoryController::class, [
        'except' => [
            'index', 'create', 'show', 'edit'
        ]
    ]);

    Route::group(['middleware' => 'groupAdmin'], function() {
        Route::get('settings/group', 'UserController@getGroupSettings');
        Route::post('settings/group', 'UserController@postGroupSettings');
    });

    Route::get('settings/profile', 'UserController@getSettings');
    Route::post('settings/profile', 'UserController@postSettings');

	Route::get("export", "ExportController@index");
    Route::post('export', 'ExportController@process');

    /**
     * API routes
     */
    Route::group(["prefix" => "api"], function() {
        Route::get('transactions', function(\Illuminate\Contracts\Auth\Guard $auth) {

            $data = $auth->user()->group->transactions()
                ->with('category', 'transaction_type', 'user')
                ->get();

            return [
                'draw' => 1,
                'recordsTotal' => $data->count(),
                'recordsFiltered' => $data->count(),
                'data' => $data
            ];
        });

        Route::get('categories', function() {
            return response()->json( \App\Category::authorized()->get() );
        });
    });
});
