<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class SaveTransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return empty($this->id) || $this->user_id == Auth::user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'short_description' => 'required',
            'date' => 'required|date',
            'amount' => 'required|numeric',
            'transaction_type_id' => 'required|numeric'
        ];
    }
}
