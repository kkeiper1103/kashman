<?php

namespace App\Http\Requests;

use Auth;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (int) Auth::user()->id === (int) $this->request->get('id');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'current_password' => 'required_with:password|verified',
            'password' => 'min:8|confirmed',
            'password_confirmation' => 'required_with:password'
        ];
    }
}
