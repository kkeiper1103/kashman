<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSettings()
    {
        return view('users.settings');
    }

    /**
     * @param Requests\UpdateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSettings(Requests\UpdateUserRequest $request)
    {
        $user = User::find($request->get('id'));

        $user->email = $request->get('email', $user->email);
        $user->name = $request->get('name', $user->name);

        if( $request->has('password') )
            $user->password = Hash::make($request->get('password'));

        if( $user->save() ) {
            session()->flash('success', 'Updated User Settings!');
        }
        else {
            session()->flash('danger', 'Could Not Update User\'s Settings!');
        }

        return redirect()->to('settings/profile');
    }

    //

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getGroupSettings()
    {
        return view('users.group-settings')->withGroup(Auth::user()->group);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postGroupSettings( Request $request )
    {
        $group = Auth::user()->group;


        if( $group->fill($request->all())->save() )
        {
            session()->flash('success', 'Updated Group Settings!');
        }
        else
        {
            session()->flash('danger', 'Could Not Update Group Settings!');
        }


        return redirect()->to('settings/group');
    }
}
