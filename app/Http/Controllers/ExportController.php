<?php

namespace App\Http\Controllers;

use App\Exporter\CsvExporter;
use App\Exporter\JsonExporter;
use App\Exporter\XlsExporter;
use App\Exporter\XlsxExporter;
use App\Exporter\XmlExporter;
use Illuminate\Http\Request;

use App\Http\Requests;
use InvalidArgumentException;

class ExportController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        return view('export.index');
    }

    /**
     * @param Request $request
     */
    public function process(Request $request)
    {
        switch($request->input('format')) {
            case 'csv':
                $type = CsvExporter::class;
                break;

            case 'excel2003':
                $type = XlsExporter::class;
                break;

            case 'excel2007':
                $type = XlsxExporter::class;
                break;

            case 'json':
                $type = JsonExporter::class;
                break;

            case 'xml':
                $type = XmlExporter::class;
                break;

            default:
                throw new InvalidArgumentException("{$request->input('format')} Is Not a Valid Export Format.");
                break;
        }

        return with(new $type)->export($request);
    }
}
