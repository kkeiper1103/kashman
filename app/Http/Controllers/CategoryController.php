<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\SaveCategoryRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SaveCategoryRequest $request)
    {
        $category = Category::create($request->except(['_token', '_method']));

        return redirect()->route('transactions.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\SaveCategoryRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\SaveCategoryRequest $request, $id)
    {
        $category = Category::find($id);

        $category->fill($request->except(['_token', '_method']))->save();

        return redirect()->route('transactions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        return redirect()->route('transactions.index');
    }
}
