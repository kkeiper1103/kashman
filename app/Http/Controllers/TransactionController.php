<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use Auth;
use DB;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;

class TransactionController extends Controller
{
    /**
     * @param Guard $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function getJson(Guard $auth)
    {
        return response()->json( $auth->user()->group->transactions );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Guard $auth)
    {
        return view('transactions.index')
            ->withTransactions( $auth->user()->group->transactions()->with('category', 'transaction_type', 'user')->orderBy('date', 'desc')->get() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\SaveTransactionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SaveTransactionRequest $request)
    {
        return DB::transaction(function() use($request){
            /**
             * @var $transaction Transaction
             */
            $transaction = Auth::user()->transactions()
                ->create($request->except(['_token', '_method']));

            $transaction->calculateRrule( $request );

            return $request->ajax() ?
                response()->json($transaction) :
                redirect()->route('transactions.index');
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\SaveTransactionRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\SaveTransactionRequest $request, $id)
    {
        return DB::transaction(function() use($request, $id){
            /**
             * @var $transaction Transaction
             */
            $transaction = Auth::user()->transactions()
                ->find($id);


            $transaction->fill($request->all())->save();


            $transaction->calculateRrule( $request );

            return $request->ajax() ?
                response()->json($transaction) :
                redirect()->route('transactions.index');
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
