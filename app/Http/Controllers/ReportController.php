<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reports.index')
            ->withTransactions($this->getTransactions());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $transactions = $this->getTransactions();

        return response()->json($transactions);
    }

    /**
     * 
     */
    protected function getTransactions()
    {
        return Auth::user()->group->transactions()->with('user', 'transaction_type', 'category')->get();
    }
}
