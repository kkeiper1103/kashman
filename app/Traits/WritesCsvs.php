<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/14/16
 * Time: 8:49 AM
 */

namespace App\Traits;


use Illuminate\Support\Collection;
use League\Csv\Writer;
use SplTempFileObject;

trait WritesCsvs
{
    /**
     * @param $data array|Collection
     * @return int
     */
    public function writeCsvFromAssoc( $data ) {
        $csv = Writer::createFromFileObject(new SplTempFileObject());


        if( $data instanceof Collection ) {
            $data = $data->toArray();
        }

        /**
         * @param $el
         * @return array
         */
        $fn = function($el) {
            return (array) $el;
        };

        /**
         * @param $el
         * @return array
         */
        $fnHeaders = function($el) {
            return array_keys( (array) $el );
        };

        $csv->insertOne( $fnHeaders( current($data) ) );

        $csv->insertAll( array_map($fn, $data) );


        ob_start();
            $csv->output();
        return ob_get_clean();
    }
}