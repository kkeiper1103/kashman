<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 9:03 AM
 */

namespace App\Composers\Charts;


use App\Composers\ComposerInterface;
use Auth;
use DB;
use Illuminate\View\View;

class IncomeComposer implements ComposerInterface
{

    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $transactions = DB::table('transactions')
            ->join('categories', 'categories.id', '=', 'transactions.category_id')
            ->join('transaction_types', 'transaction_types.id', '=', 'transactions.transaction_type_id')
            ->select('transactions.*', 'categories.name', 'transaction_types.type')
            ->whereIn('transactions.user_id', Auth::user()->group->users()->pluck('id'))
            ->where('transaction_types.type', '=', 'income')
            ->get();

        $view->withTransactions( collect($transactions) );
    }
}