<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 8:57 AM
 */

namespace App\Composers\Charts;


use App\Composers\ComposerInterface;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\SQLiteConnection;
use Illuminate\Http\Request;
use Illuminate\View\View;
use NumberFormatter;

class GrossComposer implements ComposerInterface
{
    /**
     * @var NumberFormatter
     */
    private $formatter;
    /**
     * @var Request
     */
    private $request;

    /**
     * GrossComposer constructor.
     * @param NumberFormatter $formatter
     * @param Request $request
     */
    public function __construct(NumberFormatter $formatter, Request $request)
    {

        $this->formatter = $formatter;
        $this->request = $request;
    }

    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $isSqlite = DB::connection() instanceof SQLiteConnection;

        $amountColumn = $isSqlite ?
            "(t.amount * case tt.type when 'expense' then -1 else 1 end)" :
            "(t.amount * if(tt.type = 'expense', -1, 1))";

        $data = collect(DB::select("
            select t.date, t.short_description as 'label', u.name, tt.type, 
            $amountColumn as 'amount'
            
            from transactions t
            join users u on t.user_id = u.id
            join groups g on u.group_id = g.id
            join transaction_types tt on t.transaction_type_id = tt.id
            where g.id = ? and date >= ?
            order by date asc
        ", [
            Auth::user()->group->id,
            Carbon::now()->subMonth(3)
        ]));

        $total = 0;
        $data->transform(function($item) use(&$total) {
            $total += $item->amount;

            $item->data = $total;
            return $item;
        });

        $view->withData($data);
    }
}