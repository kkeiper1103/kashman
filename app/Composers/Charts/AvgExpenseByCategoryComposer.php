<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 9:04 AM
 */

namespace App\Composers\Charts;


use App\Composers\ComposerInterface;
use DB;
use Illuminate\View\View;
use NumberFormatter;

class AvgExpenseByCategoryComposer implements ComposerInterface
{
    /**
     * @var NumberFormatter
     */
    private $formatter;

    /**
     * AvgExpenseByCategoryComposer constructor.
     * @param NumberFormatter $formatter
     */
    public function __construct(NumberFormatter $formatter)
    {

        $this->formatter = $formatter;
    }

    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $categories = collect(DB::select(
            'SELECT DISTINCT c.name AS "label", AVG(t.amount) AS "data"
            FROM categories c
            LEFT JOIN transactions t ON t.category_id = c.id
            LEFT JOIN transaction_types tt ON tt.id = t.transaction_type_id
            WHERE tt.type = \'expense\'
            GROUP BY c.name
            ORDER BY AVG(t.amount) DESC;'
        ));

        $categories->transform(function($item){
            return [
                'label' => $item->label,
                'value' => round(floatval($item->data), 2),
                'currency' => $this->formatter->format($item->data)
            ];
        });

        $view->withCategories( $categories );
    }
}