<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 9:00 AM
 */

namespace App\Composers\Charts;


use App\Composers\ComposerInterface;
use Carbon\Carbon;
use DB;
use Illuminate\Database\SQLiteConnection;
use Illuminate\Http\Request;
use Illuminate\View\View;
use NumberFormatter;

class ExpensesComposer implements ComposerInterface
{
    /**
     * @var NumberFormatter
     */
    private $formatter;
    /**
     * @var Request
     */
    private $request;

    /**
     * ExpensesComposer constructor.
     * @param NumberFormatter $formatter
     * @param Request $request
     */
    public function __construct(NumberFormatter $formatter, Request $request)
    {
        $this->formatter = $formatter;
        $this->request = $request;
    }

    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $isSqlite = DB::connection() instanceof SQLiteConnection;

        switch($this->request->header('X-REPORT-TYPE')) {
            case "hourly":
                $date = Carbon::now()->subWeek();
                $groupBy = $isSqlite ? 'strftime(\'%H\', date)' : "DATE_FORMAT(date, '%H')";
                break;
            case "daily":
            default:
                $date = Carbon::now()->subMonth();
                $groupBy = $isSqlite ? 'strftime(\'%d\', date)' : "DATE_FORMAT(date, '%d')";
                break;
            case "weekly":
                $date = Carbon::now()->subMonth();
                $groupBy = $isSqlite ? 'strftime("%W", date)' : "DATE_FORMAT(date, '%W')";
                break;
            case "monthly":
                $date = Carbon::now()->subYear();
                $groupBy = $isSqlite ? 'strftime("%m", date)' : "DATE_FORMAT(date, '%m')";
                break;
            case "yearly":
                $date = Carbon::now()->subYear(10);
                $groupBy = $isSqlite ? 'strftime("%Y", date)' : "DATE_FORMAT(date, '%Y')";
                break;
        }



        $transactions = DB::select("
            select sum(t.amount) as 'total', u.name as 'username', date from transactions t
            join categories c on c.id = t.category_id
            join transaction_types tt on tt.id = t.transaction_type_id
            join users u ON u.id = t.user_id
            
            where date >= ? and tt.type = 'expense'
            
            group by $groupBy
            
            order by t.date DESC;
        ", [
            $date
        ]);


        $view->withType(ucwords($this->request->header('X-REPORT-TYPE')))
            ->withTransactions( collect($transactions) );
    }
}