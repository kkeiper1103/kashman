<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 8:53 AM
 */

namespace App\Composers\Charts;

use App\Composers\ComposerInterface;
use Illuminate\View\View;
use NumberFormatter;
use DB;


class ByCategoryComposer implements ComposerInterface
{
    /**
     * @var NumberFormatter
     */
    private $formatter;

    /**
     * ByCategory constructor.
     * @param NumberFormatter $formatter
     */
    public function __construct(NumberFormatter $formatter)
    {

        $this->formatter = $formatter;
    }

    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $categories = collect(DB::select('
                select distinct c.name as "label", SUM(t.amount) as "data"
                FROM categories c
                left join transactions t ON t.category_id = c.id
                left join transaction_types tt ON tt.id = t.transaction_type_id
                WHERE tt.type = \'expense\'
                group by c.name
                order by SUM(t.amount) DESC;
            '));

        $categories->transform(function($item) {
            $item->data = round(floatval($item->data), 2);
            $item->currency = $this->formatter->format($item->data);

            return $item;
        });

        $view->withCategories( $categories );
    }
}