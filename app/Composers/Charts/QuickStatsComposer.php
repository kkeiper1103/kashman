<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 9:01 AM
 */

namespace App\Composers\Charts;


use App\Composers\ComposerInterface;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\View\View;

class QuickStatsComposer implements ComposerInterface
{

    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $stats = collect();


        $stats = $stats->merge((array) DB::select(
            'SELECT SUM(amount) as "income" FROM transactions t
                  JOIN transaction_types tt ON tt.id = t.transaction_type_id
                  WHERE t.user_id IN (?) AND tt.type = ? AND t.date >= ?',
            [implode(',', Auth::user()->group->users()->pluck('id')->all()), 'income', Carbon::now()->subMonth()])[0]
        );


        $stats = $stats->merge((array) DB::select(
            'SELECT SUM(amount) as "expense" FROM transactions t
                    JOIN transaction_types tt ON tt.id = t.transaction_type_id
                    WHERE t.user_id IN (?) AND tt.type = ? AND t.date >= ?',

            [implode(',', Auth::user()->group->users()->pluck('id')->all()), 'expense', Carbon::now()->subMonth()])[0]
        );


        $stats = $stats->merge([
            'percentage_spent' => round($stats['expense'] / max($stats['income'], .01), 3) * 100
        ]);


        $stats = $stats->merge([
            'biggest_expenses' => DB::select('
                    SELECT c.name, SUM(t.amount) as ttl_amount FROM categories c
                    JOIN transactions t ON t.category_id = c.id
                    JOIN transaction_types tt ON t.transaction_type_id = tt.id
                    WHERE tt.type = "expense" AND t.date >= ? AND t.user_id IN (?)
                    GROUP BY c.name
                    ORDER BY ttl_amount DESC LIMIT 3
                ', [Carbon::now()->subMonth(), implode(',', Auth::user()->group->users()->pluck('id')->all())])
        ]);

        $view->withStats($stats);
    }
}