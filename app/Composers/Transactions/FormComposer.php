<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 9:06 AM
 */

namespace App\Composers\Transactions;


use App\Composers\ComposerInterface;
use DB;
use Illuminate\View\View;

class FormComposer implements ComposerInterface
{

    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        $options = DB::table('transactions')
            ->select('short_description')
            ->distinct()
            ->orderBy('short_description', 'asc')
            ->pluck('short_description');

        $view->withShortOptions($options);
    }
}