<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/1/16
 * Time: 8:53 AM
 */

namespace App\Composers;


use Illuminate\View\View;

interface ComposerInterface
{
    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view);
}