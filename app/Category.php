<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return $this->id ?
            parent::toJson($options) :
            json_encode($this->getEmptyModel(), $options);
    }

    /**
     *
     */
    public function getEmptyModel() {
        return [
            'name' => null
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAuthorized($query) {
        return $query->orderBy('name', 'ASC')
            ->whereNull('user_id')
            ->orWhere('user_id', '=', Auth::id());
    }
}
