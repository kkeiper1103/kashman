<?php

namespace App\Providers;

use App\Composers\Charts\AvgExpenseByCategoryComposer;
use App\Composers\Charts\ByCategoryComposer;
use App\Composers\Charts\ExpensesComposer;
use App\Composers\Charts\GrossComposer;
use App\Composers\Charts\IncomeComposer;
use App\Composers\Charts\QuickStatsComposer;
use App\Composers\Transactions\FormComposer;


use Illuminate\Support\ServiceProvider;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('reports.charts.by-category', ByCategoryComposer::class);
        view()->composer('reports.charts.gross', GrossComposer::class);
        view()->composer('reports.charts.expenses', ExpensesComposer::class);
        view()->composer('reports.charts.quick-stats', QuickStatsComposer::class);
        view()->composer('reports.charts.income', IncomeComposer::class);
        view()->composer('reports.charts.avg-expense-by-category', AvgExpenseByCategoryComposer::class);

        view()->composer('transactions._form', FormComposer::class);
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
