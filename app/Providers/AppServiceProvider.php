<?php

namespace App\Providers;

use Auth;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Hash;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        require_once base_path('bootstrap/helpers.php');

        \Validator::extend('verified', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->getAuthPassword());
        });


        config([
            'timezone' => Auth::check() ?
                Auth::user()->timezone :
                env('TIMEZONE', 'UTC')
        ]);

        date_default_timezone_set(config('timezone'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\NumberFormatter::class, function() {
            return new \NumberFormatter(env("CURRENCY_FORMAT"), \NumberFormatter::CURRENCY);
        });


        if( app()->environment() == "local" )
        {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
    }
}
