<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/14/16
 * Time: 1:40 PM
 */

namespace App\Exporter;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;

class XlsExporter extends BaseExporter
{
    /**
     * @var LaravelExcelWriter
     */
    protected $excel;

    /**
     * XlsExporter constructor.
     */
    public function __construct()
    {
        $date = Carbon::now()->toIso8601String();

        $this->excel = Excel::create("export-{$date}");
    }

    /**
     * @param Request $settings
     */
    function exportCategories(Request $settings)
    {
        $categories = $this->getCategories();

        $this->excel->sheet(
            "Categories",
            function(LaravelExcelWorksheet $excel) use($categories){
                $excel->fromArray($categories);
            }
        );
    }

    /**
     * @param Request $settings
     */
    function exportIncome(Request $settings)
    {
        $transactions = $this->getIncomeTransactions();

        $this->excel->sheet(
            'Income',
            function($excel) use($transactions) {
                $excel->fromArray($transactions);
            }
        );
    }

    /**
     * @param Request $settings
     */
    function exportExpense(Request $settings)
    {
        $transactions = $this->getExpenseTransactions();
        $this->excel->sheet(
            'Expenses',
            function($excel) use($transactions) {
                $excel->fromArray($transactions);
            }
        );
    }

    /**
     *
     */
    public function output()
    {
        $this->excel->download();
    }
}