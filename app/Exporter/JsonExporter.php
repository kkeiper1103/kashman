<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/15/16
 * Time: 1:36 PM
 */

namespace App\Exporter;



use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class JsonExporter
 * @package App\Exporter
 */
class JsonExporter extends BaseExporter
{
    protected $data = [];

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function output()
    {
        $json = json_encode($this->data);

        header('Content-Type: application/json');
        header('Content-Length: ' . strlen($json));

        $date = Carbon::now()->toIso8601String();

        header("Content-Disposition: attachment; filename=\"export-{$date}.json\"");
        exit($json);
    }

    /**
     * @param Request $settings
     */
    function exportCategories(Request $settings)
    {
        $this->data['categories'] = $this->getCategories();
    }

    /**
     * @param Request $settings
     */
    function exportIncome(Request $settings)
    {
        $this->data['income'] = $this->getIncomeTransactions();
    }

    /**
     * @param Request $settings
     */
    function exportExpense(Request $settings)
    {
        $this->data['expenses'] = $this->getExpenseTransactions();
    }
}