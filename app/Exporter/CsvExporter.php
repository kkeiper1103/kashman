<?php
/**
 * Created by PhpStorm.
 * User: kkeiper
 * Date: 7/8/2016
 * Time: 10:08 PM
 */

namespace App\Exporter;


use App\Category;
use App\Traits\WritesCsvs;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use ZipArchive;

class CsvExporter extends BaseExporter
{
    use WritesCsvs;
    /**
     * @var ZipArchive
     */
    private $archive;

    /**
     * @var string
     */
    private $tmpFile;

    /**
     * CsvExporter constructor.
     */
    public function __construct()
    {
        $this->archive = new ZipArchive();

        $this->tmpFile = tempnam(sys_get_temp_dir(), 'zip-');

        $this->archive->open($this->tmpFile, ZipArchive::CREATE);
    }

    /**
     * @param Request $settings
     */
    public function exportIncome( Request $settings )
    {
        $transactions = DB::select('
            select t.*, u.name as \'username\', c.name as "category_name" from transactions t
            join categories c on c.id = t.category_id
            join transaction_types tt on tt.id = t.transaction_type_id
            join users u ON u.id = t.user_id
            
            where tt.type = \'income\'
            
            order by t.date DESC;
        ', [  ]);

        $this->archive->addFromString(
            "income.csv",
            $this->writeCsvFromAssoc($transactions)
        );
    }

    /**
     * @param Request $settings
     */
    public function exportExpense( Request $settings )
    {
        $transactions = DB::select('
            select t.*, u.name as \'username\', c.name as "category_name" from transactions t
            join categories c on c.id = t.category_id
            join transaction_types tt on tt.id = t.transaction_type_id
            join users u ON u.id = t.user_id
            
            where tt.type = \'expense\'
            
            order by t.date DESC;
        ');

        $this->archive->addFromString(
            "expense.csv",
            $this->writeCsvFromAssoc($transactions)
        );
    }

    /**
     * @param Request $settings
     */
    function exportCategories(Request $settings)
    {
        /**
         *
         */
        $this->archive->addFromString(
            "categories.csv",
            $this->writeCsvFromAssoc( Category::authorized()->get() )
        );
    }

    /**
     *
     */
    public function output()
    {
        $this->archive->close();

        header('Content-Type: application/zip');
        header('Content-Length: ' . filesize($this->tmpFile));

        $date = Carbon::now()->toIso8601String();

        header("Content-Disposition: attachment; filename=\"export-{$date}.zip\"");
        readfile($this->tmpFile);
        unlink($this->tmpFile);
    }
}