<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/15/16
 * Time: 1:29 PM
 */

namespace App\Exporter;


class XlsxExporter extends XlsExporter
{
    /**
     * Only thing we need to override is the format!
     */
    public function output()
    {
        $this->excel->download('xlsx');
    }
}