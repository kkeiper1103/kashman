<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/15/16
 * Time: 2:12 PM
 */

namespace App\Exporter;


use Carbon\Carbon;
use Illuminate\Http\Request;

class XmlExporter extends BaseExporter
{
    protected $writer;

    /**
     * XmlExporter constructor.
     */
    public function __construct()
    {
        $this->writer = new \XMLWriter();

        $this->writer->openMemory();
        $this->writer->startDocument(null, 'UTF-8');

        $this->writer->startElement('export');
    }

    /**
     *
     */
    public function output()
    {
        $this->writer->endElement();

        $xml = $this->writer->outputMemory(true);

        header('Content-Type: text/xml');
        header('Content-Length: ' . strlen($xml));

        $date = Carbon::now()->toIso8601String();
        header("Content-Disposition: attachment; filename=\"export-{$date}.xml\"");

        exit( $xml );
    }

    /**
     * @param Request $settings
     */
    function exportCategories(Request $settings)
    {
        $this->element('categories', function() {
            foreach($this->getCategories() as $category) {

                $this->element('category', function() use($category) {
                    foreach($category->getAttributes() as $name => $value) {
                        $this->writer->writeAttribute($name, $value);
                    }
                });

            }
        });
    }

    /**
     * @param Request $settings
     */
    function exportIncome(Request $settings)
    {
        $this->element('income', function() {
            foreach($this->getIncomeTransactions() as $transaction)
            {
                $this->element('transaction', function() use($transaction){
                    foreach($transaction as $name => $value)
                        $this->writer->writeAttribute($name, $value);
                });
            }
        });
    }

    /**
     * @param Request $settings
     */
    function exportExpense(Request $settings)
    {
        $this->element('expenses', function() {
            foreach($this->getExpenseTransactions() as $transaction)
            {
                $this->element('transaction', function() use($transaction){
                    foreach($transaction as $name => $value)
                        $this->writer->writeAttribute($name, $value);
                });
            }
        });
    }

    /**
     * @param string $name
     * @param callable $fn
     */
    protected function element($name, $fn)
    {
        $this->writer->startElement($name);
        call_user_func($fn);
        $this->writer->endElement();
    }
}