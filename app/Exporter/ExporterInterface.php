<?php
/**
 * Created by PhpStorm.
 * User: kkeiper
 * Date: 7/8/2016
 * Time: 10:07 PM
 */

namespace App\Exporter;


use Illuminate\Http\Request;

interface ExporterInterface
{
    function export(Request $settings);

    function exportCategories(Request $settings);
    function exportIncome(Request $settings);
    function exportExpense(Request $settings);
}