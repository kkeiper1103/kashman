<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 7/15/16
 * Time: 11:54 AM
 */

namespace App\Exporter;


use App\Category;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

abstract class BaseExporter implements ExporterInterface
{
    abstract public function output();

    /**
     * @param Request $settings
     */
    public function export( Request $settings )
    {
        foreach( $settings->input('types', []) as $method ) {
            $method = 'export' . ucfirst($method);

            if( !method_exists($this, $method) ) {
                throw new \InvalidArgumentException("$method is an invalid transaction type!");
            }

            call_user_func_array([$this, $method], [$settings]);
        }

        $this->exportCategories($settings);

        $this->output();
    }

    /**
     * @return Collection
     */
    public function getCategories()
    {
        return Category::authorized()->get();
    }

    /**
     * @return array
     */
    public function getIncomeTransactions()
    {
        return array_map('object2array', DB::select('
            select t.*, u.name as \'username\', c.name as "category_name" from transactions t
            join categories c on c.id = t.category_id
            join transaction_types tt on tt.id = t.transaction_type_id
            join users u ON u.id = t.user_id

            where tt.type = \'income\'

            order by t.date DESC;
        '));
    }

    /**
     * @return array
     */
    public function getExpenseTransactions()
    {
        return array_map('object2array', DB::select('
            select t.*, u.name as \'username\', c.name as "category_name" from transactions t
            join categories c on c.id = t.category_id
            join transaction_types tt on tt.id = t.transaction_type_id
            join users u ON u.id = t.user_id

            where tt.type = \'expense\'

            order by t.date DESC;
        '));
    }
}