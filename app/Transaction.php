<?php

namespace App;

use App\Http\Requests\SaveTransactionRequest;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Model;
use When\When;

class Transaction extends Model
{
    protected $dates = [
        'date'
    ];

    protected $appends = [
        'type', 'category_name'
    ];

    protected $fillable = [
        'short_description', 'date', 'amount', 'description',
        'repeats', 'rrule',
        'transaction_type_id', 'category_id'
    ];


    public function toJson($options = 0)
    {
        return $this->id ?
            parent::toJson($options) :
            json_encode($this->getEmptyModel(), $options);
    }

    /**
     *
     */
    public function getEmptyModel() {
        return [
            'short_description' => null,
            'amount' => '0.00',
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'transaction_type_id' => 1,
            'category_id' => 1
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function getTypeAttribute()
    {
        return $this->transaction_type ? $this->transaction_type->type : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction_type()
    {
        return $this->belongsTo(TransactionType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return mixed
     */
    public function getCategoryNameAttribute()
    {
        return $this->category ? $this->category->name : null;
    }

    /**
     * @param SaveTransactionRequest $request
     * @throws \Exception
     * @throws \When\InvalidStartDate
     */
    public function calculateRrule(SaveTransactionRequest $request)
    {
        // shortcircuit any rrules that don't need generated
        if( $request->get('repeats', false) == false ) {
            return;
        }

        $this->rrule = $this->generateRepeatRules($request);

        $r = new When();
        $r->startDate(new DateTime($this->date))
            ->rrule($this->rrule)
            ->generateOccurrences();


        foreach($r->occurrences as $occurrence)
        {
            $t = new Transaction($this->toArray());

            $t->date = Carbon::instance($occurrence);

            var_dump($t->toArray());
        }


        throw new \Exception("Preventing Transaction From Saving.");
    }

    /**
     * @param SaveTransactionRequest $request
     * @return string
     */
    public function generateRepeatRules( SaveTransactionRequest $request )
    {
        $rules = array_filter([
            'until'         => $request->get('until', null),
            'freq'          => $request->get('frequency', null),
            'count'         => $request->get('count', 1),
            'interval'      => $request->get('interval', null),
            'wkst'          => $request->get('wkst', null),

            'byday'         => $request->get('byday', null),
            'bymonthday'    => $request->get('bymonthday', null),
            'byyearday'     => $request->get('byyearday', null),
            'byweekno'      => $request->get('byweekno', null),
            'bymonth'       => $request->get('bymonth', null),
            'bysetpos'      => $request->get('bysetpos', null),
            'byhour'        => $request->get('byhour', null),
            'byminute'      => $request->get('byminute', null),
            'bysecond'      => $request->get('bysecond', null)
        ]);

        return implode(';', array_map(function($v, $k) {
            return strtoupper("$k=$v");
        }, $rules, array_keys($rules)));
    }
}
