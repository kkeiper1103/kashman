@extends('layouts.application')

@section("title", "Exporting Transactions")

@section('breadcrumbs')
    @parent

    <li class="active">
        <i class="fa fa-exchange"></i> Export Transactions
    </li>
@endsection

@section('content')

    <div class="row">
        <form action="{{ url('export') }}" method="post" class="col-xs-6">
            {!! csrf_field() !!}

            <fieldset>
                <legend>Main Settings</legend>

                <div class="form-group">
                    <label for="format" class="control-label">Format</label>
                    <select name="format" id="format" class="form-control">
                        <option value="csv">CSV</option>
                        <option value="excel2003">Excel 2003 (.xls)</option>
                        <option value="excel2007">Excel 2007 (.xlsx)</option>
                        <option value="json">JSON</option>
                        <option value="xml">XML</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Transaction Types</label>
                    <br>

                    <label class="control-label">
                        <input type="checkbox" name="types[]" value="income"> Income
                    </label>
                    <label class="control-label">
                        <input type="checkbox" name="types[]" value="expense"> Expense
                    </label>
                </div>
            </fieldset>


            <fieldset>
                <legend>Transaction Range</legend>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="start_date" class="control-label">Start Date</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                <input type="datetime" id="start_date" name="start_date" class="form-control">
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="end_date" class="control-label">End Date</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                <input type="datetime" id="end_date" name="end_date" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

            </fieldset>


            <div class="form-group">
                <button class="btn btn-primary">Export</button>
            </div>
        </form>
    </div>

@stop