@extends('layouts.application')

@section('title', 'All Transactions')


@section('breadcrumbs')
    @parent

    <li class="active">
        <i class="fa fa-list-alt"></i> All Transactions
    </li>
@endsection


@push('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
@endpush


@section('content')
    <div class="row">
        <div class="col-sm-3">

            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">New Transaction</h3>
                </div>
                <div class="panel-body">
                    @include("transactions._form", ['transaction' => new \App\Transaction])
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">New Transaction Category</h3>
                </div>
                <div class="panel-body">
                    @include("transactions._category_form", ['category' => new \App\Category])
                </div>
            </div>
        </div>
        <div class="col-sm-9">

            @include('transactions._table', ['transactions' => $transactions])

        </div>
    </div>
@endsection