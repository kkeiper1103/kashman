<form action="{{ empty($category->id) ? route('categories.store') : route('categories.update', $category->id) }}"
      method="{{ empty($category->id) ? 'post' : 'put' }}" class="form" id="category-form" @submit.prevent='saveCategory'>
    {!! csrf_field() !!}
    {!! method_field(empty($category->id) ? 'post' : 'put') !!}

    <div class="form-group" :class="{ 'has-error': errors.name }">
        <label for="name" class="control-label">Name</label>

        <input type="text" id="name" name="name" class="form-control"
               v-model='category.name' placeholder="Category Name" />

        <template v-if="errors.name">
            <span class="help-block">@{{ errors.name }}</span>
        </template>
    </div>

    <button class="btn btn-primary btn-block">Save Category</button>
</form>

@push('scripts')
<script>
    jQuery(function($) {

        var categoryForm = new Vue({
            el: "#category-form",
            parent: bus,

            data: {
                category: {!! $category !!},
                errors: {}
            },

            methods: {
                saveCategory: function() {
                    var $form = $(this.$el),
                        method = $form.attr('method'),
                        action = $form.attr('action');


                    this.$http[method](action, this.category).then(function(response) {
                        //success

                        bus.$broadcast('categories-updated', response.data);
                        bus.$broadcast('update-alerts', {
                            type: 'success',
                            message: 'Successfully Added New Category!'
                        });

                        this.errors = {};
                        this.category = {!! $category !!};

                    }, function(response) {
                        //error

                        this.errors = response.data;
                    });
                }
            }
        });

    });
</script>
@endpush