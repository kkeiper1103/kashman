<div class="form-group{{ $errors->has('repeats') ? ' has-error' : '' }}">
    <input type="hidden" value="0" name="repeats">

    <label for="repeats" class="control-label">
        <input type="checkbox" value="1" name="repeats" id="repeats" {{ $transaction->repeats ? "checked" : "" }}> Repeats?
    </label>

    @if( $errors->has('repeats') )
        <span class="help-block">{{ $errors->first('repeats') }}</span>
    @endif
</div>

<div class="form-group{{ $errors->has('frequency') ? ' has-error' : '' }}">
    <label for="frequency" class="control-label">Frequency</label>

    <select name="frequency" id="frequency" class="form-control">
        <option value="secondly">By the Second</option>
        <option value="minutely">By the Minute</option>
        <option value="hourly">Hourly</option>
        <option value="daily" {{ empty($transaction->rrule) ? 'selected' : '' }}>Daily</option>
        <option value="weekly">Weekly</option>
        <option value="monthly">Monthly</option>
        <option value="yearly">Yearly</option>
    </select>

    @if( $errors->has('frequency') )
        <span class="help-block">{{ $errors->first('frequency') }}</span>
    @endif
</div>

<div class="form-group{{ $errors->has('interval') ? ' has-error' : '' }}">
    <label for="interval" class="control-label">Interval</label>

    <input type="text" id="interval" name="interval" class="form-control" value="{{ old('interval', 1) }}">

    @if( $errors->has('interval') )
        <span class="help-block">{{ $errors->first('interval') }}</span>
    @endif
</div>


<div>
    <style scoped>
        p .form-control {
            width: auto;
            display: inline;
            padding: 2px;
            text-align: center;
        }

        p input.sm {
            width: 30px;
        }
    </style>

    <p><input type="text" name="count" value="{{ old('interval', 1) }}" class="form-control sm"> times,
        until
        <span class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" name="until" value="{{ old('until', date("Y-m-d H:i:s")) }}" class="form-control">
        </span>,
        <br>every
            <input type="text" name="interval" value="{{ old('interval', 1) }}" class="form-control sm">
        <select name="freq" id="freq" class="form-control">
            <option value="daily">Days</option>
            <option value="weekly">Weeks</option>
            <option value="monthly">Months</option>
        </select>.</p>

    <p><input type="checkbox" name="date"> on
        <select name="byday" class="form-control">
            <option value="su">Sunday</option>
            <option value="mo">Monday</option>
            <option value="tu">Tuesday</option>
            <option value="we">Wednesday</option>
            <option value="th">Thursday</option>
            <option value="fi">Friday</option>
            <option value="sa">Saturday</option>
        </select>
    </p>

    <p><input type="checkbox">
        only when the date is <input type="text" name="bymonthday" class="form-control sm" value="1st">
    </p>
</div>

