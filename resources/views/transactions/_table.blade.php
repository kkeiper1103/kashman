<table class="table table-bordered table-striped table-hover" id="transactions-table" data-order='[[ 2, "desc" ]]' data-page-length='10'>
    <thead>
    <tr>
        <th>User</th>
        <th>Short Description</th>
        <th>Date</th>
        <th>Transaction Type</th>
        <th>Amount</th>
        <th>Category</th>
        <th>Actions</th>
        <th></th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>User</th>
        <th>Short Description</th>
        <th>Date</th>
        <th>Transaction Type</th>
        <th>Amount</th>
        <th>Category</th>
        <th>Actions</th>
        <th></th>
    </tr>
    </tfoot>
</table>

@push('scripts')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
    jQuery(function($){
        var table = $('#transactions-table').DataTable({
            columns: [
                { data: 'user.name' },
                { data: 'short_description' },
                { data: 'date' },
                { data: 'type' },
                {
                    data: 'amount',
                    render: function(amount, method, original, table) {
                        var number = parseFloat(amount);

                        if( original.transaction_type.type == 'expense' ) {
                            number *= -1;
                        }

                        return number.toFixed(2)
                                .replace(/\-/gi, '-$')
                                .replace(/^(\d)/gi, '$$$1');
                    }
                },
                { data: 'category_name' },
                {
                    data: 'id',
                    render: function(id) {
                        return "<span @click='editTransaction("+id+")'><i class='fa fa-edit'></i> Edit</span>";
                    }
                },
                {
                    data: 'id',
                    render: function(id) {
                        return "<span @click='deleteTransaction("+id+")'><i class='fa fa-trash-o'></i> Delete</span>";
                    }
                }
            ],
            ajax: 'api/transactions'
        });


        var vm = new Vue({
            el: '#transactions-table',
            parent: bus,

            data: {
                transactions: {!! $transactions !!}
            },

            methods: {
                editTransaction: function(id) {
                    console.log(id);
                    bus.$broadcast('edit', id);
                },
                deleteTransaction: function(id) {
                    bus.$broadcast('delete', id);
                }
            },

            events: {
                'update-transactions': function() {
                    table.ajax.reload(null, false);
                }
            }
        });

    });
</script>
@endpush