<form action="{{ empty($transaction->id) ? route('transactions.store') : route('transactions.update', $transaction->id) }}"
      method="{{ empty($transaction->id) ? 'post' : 'put' }}" class="form" id="transaction-form" @submit.prevent="onSave">
    {!! csrf_field() !!}
    {!! method_field(empty($transaction->id) ? 'post' : 'put') !!}

    <div class="form-group" :class="{ 'has-error': errors.short_description }">
        <label for="short_description" class="control-label">Short Description</label>

        <input type="text" id="short_description" name="short_description" class="form-control"
               list="shorts" v-model="transaction.short_description" placeholder="Transaction Title" />

        <datalist id="shorts">
            <option v-for="o in short_options" :value="o">@{{ o }}</option>
        </datalist>

        <template v-if="errors.short_description">
            <span class="help-block">@{{ errors.short_description }}</span>
        </template>
    </div>


    <div class="form-group" :class="{ 'has-error': errors.date }">
        <label for="date" class="control-label">Date</label>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

            <input type="datetime" id="date" name="date" class="form-control" v-model="transaction.date">
        </div>

        <template v-if="errors.date">
            <span class="help-block">@{{ errors.date }}</span>
        </template>
    </div>


    <div class="form-group" :class="{ 'has-error': errors.amount }">
        <label for="amount" class="control-label">Amount</label>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>

            <input type="text" id="amount" name="amount" class="form-control" v-model="transaction.amount">
        </div>

        <template v-if="errors.amount">
            <span class="help-block">@{{ errors.amount }}</span>
        </template>
    </div>


    <div class="form-group" :class="{ 'has-error': errors.transaction_type_id }">
        <label for="transaction_type_id" class="control-label">Transaction Type</label>

        <select name="transaction_type_id" id="transaction_type_id" class="form-control" v-model="transaction.transaction_type_id">
            <option v-for="tt in transaction_types" :value="tt.id">@{{ tt.type }}</option>
        </select>

        <template v-if="errors.transaction_type_id">
            <span class="help-block">@{{ errors.transaction_type_id }}</span>
        </template>
    </div>


    <div class="form-group" :class="{ 'has-error': errors.category_id }">
        <label for="category_id" class="control-label">Category</label>

        <select name="category_id" id="category_id" class="form-control" v-model="transaction.category_id">
            <option v-for="c in categories" :value="c.id">@{{ c.name }}</option>
        </select>


        <span class="help-block" v-if="errors.category_id">@{{ errors.category_id }}</span>
    </div>

    <button class="btn btn-primary btn-block">Save Transaction</button>
</form>


@push('scripts')
<script>
    jQuery(function($) {
        var url = '{{ url('transactions') }}/{id}';


        var transactionForm = new Vue({
            el: "#transaction-form",
            parent: bus,

            data: {
                transaction: {!! $transaction !!},
                transaction_types: {!! App\TransactionType::all() !!},
                categories: {!! App\Category::authorized()->get() !!},
                short_options: {!! json_encode($short_options) !!},
                errors: {}
            },

            events: {
                'edit': 'onEdit',
                'delete': 'onDelete',
                'categories-updated': function() {
                    this.$http.get('api/categories').then(function(response) {
                        this.categories = response.data;
                    }, function() {

                    });
                }
            },

            methods: {
                "onSave": function() {
                    var $form = $(this.$el),
                            method = $form.attr('method'),
                            action = $form.attr('action');

                    this.$http[method](action, this.transaction).then(function(response) {
                        //success

                        bus.$broadcast('update-transactions');
                        bus.$broadcast('update-alerts', {
                            type: 'success',
                            message: 'Successfully Added New Transaction!'
                        });

                        this.transaction = {!! $transaction !!};
                    }, function(response) {
                        //error

                        this.errors = response.data;
                    });
                },

                "onEdit": function( transaction ) {
                    var $form = $(this.$el);

                    $form.find('[name=_method]').val('put');
                    $form.attr('method', 'put');
                    $form.attr('action', url.replace('{id}', transaction.id));

                    this.transaction = transaction;
                },

                "onDelete": function( id ) {

                }
            }
        });


    });
</script>
@endpush