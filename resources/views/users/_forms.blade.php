<form action="{{ url('settings/profile') }}" method="post">
    {!! csrf_field() !!}

    <input type="hidden" name="id" id="id" value="{{ $user->id }}">

    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Name</label>

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>

                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name', $user->name) }}" />
                </div>


                @if($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">Email Address</label>

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email', $user->email) }}" />
                </div>


                @if($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                <label for="current_password" class="control-label">Current Password</label>

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                    <input type="password" name="current_password" id="current_password" class="form-control" />
                </div>

                @if($errors->has('current_password'))
                    <span class="help-block">{{ $errors->first('current_password') }}</span>
                @endif
            </div>


            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                    <input type="password" name="password" id="password" class="form-control" />
                </div>

                @if($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password_confirmation" class="control-label">Password Confirmation</label>

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" />
                </div>

                @if($errors->has('password_confirmation'))
                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                @endif
            </div>
        </div>
    </div>



    <div class="form-group">
        <button class="btn btn-primary">Update Settings</button>
    </div>
</form>