@extends('layouts.application')

@section('title', 'Group Settings')

@section('breadcrumbs')
    @parent

    <li class="active">
        <i class="fa fa-group"></i> Group Settings
    </li>
@stop

@section('content')
    <form action="{{ url('settings/group') }}" method="post">
        {!! csrf_field() !!}


        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="control-label">Group Name</label>

            <input type="text" name="name" id="name" class="form-control" value="{{ old('name', $group->name) }}" />

            @if($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>


        <div class="form-group{{ $errors->has('administrators') ? ' has-error' : '' }}">
            <label class="control-label">Administrators</label>

            @if($errors->has('administrators'))
                <span class="help-block">{{ $errors->first('administrators') }}</span>
            @endif


            <div class="row">
            @foreach($group->users as $user)
                <div class="col-xs-3">
                    <label>
                        <input type="checkbox" value="{{ $user->id }}" name="administrators[]"
                               id="administrators_{{ $user->id }}" {{ $user->admin ? ' checked' : '' }}> {{ $user->name }}
                    </label>
                </div>
            @endforeach
            </div>
        </div>


        <div class="form-group">
            <button class="btn btn-primary">Update Group Settings</button>
        </div>
    </form>
@stop