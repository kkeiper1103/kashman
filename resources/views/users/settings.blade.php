@extends('layouts.application')

@section('title', 'Profile Settings')

@section('breadcrumbs')
    @parent

    <li class="active">
        <i class="fa fa-cogs"></i> Profile Settings
    </li>
@endsection


@section('content')
    @include('users._forms', ['user' => Auth::user()])
@stop