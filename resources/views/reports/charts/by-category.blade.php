<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">Expense Categories</h3>
    </div>
    <div class="panel-body">
        <div class="flot-chart">
            <div class="flot-chart-content" id="expense-categories-chart"></div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    jQuery(function($) {

        var data = {!! $categories !!};

        var plotObj = $.plot($("#expense-categories-chart"), data, {
            series: {
                pie: {
                    show: true,
                    radius: 1
                }
            },
            grid: {
                hoverable: true
            },
            legend: {
                show: true,
                labelFormatter: function(label, obj) {
                    return label + ": $" + obj.data[0][1] + " / " +
                            obj.percent.toFixed(1) + "%";
                }
            },
            resize: true
        });
    });
</script>
@endpush