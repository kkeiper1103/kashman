<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">Average Expense Per Category</h3>
    </div>
    <div class="panel-body">
        <div class="flot-chart">
            <div class="flot-chart-content" id="avg-expense-chart"></div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    jQuery(function($) {
        Morris.Bar({
            element: 'avg-expense-chart',
            data: {!! $categories !!},
            xkey: 'label',
            ykeys: ['value'],
            labels: ['Average Expense'],

            hoverCallback: function (index, options, content, row) {
                return "<strong>" + row.label + ": </strong>" + row.currency;
            },

            xLabelAngle: 35,
            hideHover: 'auto'
        });
    });
</script>
@endpush