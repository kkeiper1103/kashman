<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">Income</h3>
    </div>
    <div class="panel-body">
        <div id="income-chart"></div>
    </div>
</div>

@push('scripts')
<script>
    jQuery(function($) {
        Morris.Line({
            element: "income-chart",

            data: {!! $transactions->toJson() !!},

            // The name of the data record attribute that contains x-visitss.
            xkey: 'date',
            // A list of names of data record attributes that contain y-visitss.
            ykeys: ['amount'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Amount'],

            // Disables line smoothing
            smooth: true,
            resize: true,
            xLabelAngle: 35,
            hideHover: 'auto'
        });
    });
</script>
@endpush