<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">Cash Flow</h3>
    </div>
    <div class="panel-body">
        <div id="gross-chart"></div>
    </div>
</div>

@push('scripts')
<script>
    jQuery(function($) {
        Morris.Line({
            element: "gross-chart",

            data: {!! $data !!},

            // The name of the data record attribute that contains x-visitss.
            xkey: 'date',
            // A list of names of data record attributes that contain y-visitss.
            ykeys: ['data'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Current Cash Value'],

            hoverCallback: function(index, options, content, row) {
                return  "<strong>" + row.label + "</strong><br />" +
                        "<em>" + row.name + "</em><br />" +
                        "Change in Cash: " + row.amount + "<br />" +
                        "Total Cash Value: " + row.data;
            },

            // Disables line smoothing
            smooth: true,
            resize: true,
            xLabelAngle: 35,
            hideHover: 'auto',

            goals: [0.0],
            goalStrokeWidth: 3,
            goalLineColors: ["#930000"]
        });
    });
</script>
@endpush