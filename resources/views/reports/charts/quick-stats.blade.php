<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Last Month's Quick Stats</h3>
    </div>
    @inject('formatter', "NumberFormatter")
    <div class="panel-body">
        <dl>
            <dt>Income</dt>
            <dd>{{ $formatter->format( $stats['income'] ) }}</dd>

            <dt>Expense</dt>
            <dd>{{ $formatter->format( $stats['expense'] ) }}</dd>

            <dt>Percentage of Income Spent</dt>
            <dd>{{ $stats['percentage_spent'] }}%</dd>

            <dt>Largest Expenditure Categories</dt>
            <dd>
                <ol>
                    @foreach($stats['biggest_expenses'] as $c)
                    <li>{{ $c->name }}: {{ $formatter->format($c->ttl_amount) }}</li>
                    @endforeach
                </ol>
            </dd>
        </dl>
    </div>
</div>
