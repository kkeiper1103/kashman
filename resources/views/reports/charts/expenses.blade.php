<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $type }} Expenses</h3>
    </div>
    <div class="panel-body">
        <div id="expenses-chart"></div>
    </div>
</div>

@push('scripts')
<script>
    jQuery(function($) {
        Morris.Line({
            element: "expenses-chart",

            data: {!! $transactions !!},

            // The name of the data record attribute that contains x-visitss.
            xkey: 'date',
            // A list of names of data record attributes that contain y-visitss.
            ykeys: ['total'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Total'],

            hoverCallback: function(index, options, content, row) {
                return  "<strong>" + row.date + "</strong>: <br />" +
                        "<em>" + row.username + "</em><br />" +
                        "$" + row.total + "<br />" +
                        "Date: " + row.date + "<br />";
            },

            // Disables line smoothing
            smooth: true,
            resize: true,
            xLabelAngle: 35,
            hideHover: 'auto'
        });
    });
</script>
@endpush