@extends('layouts.application')

@section('title', 'Reports')

@section('breadcrumbs')
    @parent

    <li class="active">
        <i class="fa fa-bar-chart"></i> Reports & Graphs
    </li>
@endsection

@push('styles')
    <link href="{{ url('css/plugins/morris.css') }}" rel="stylesheet">
<link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
<link rel="stylesheet" href="{{ url('css/plugins/chartist/chartist-plugin-tooltip.css') }}">
<style>
    .ct-zoom-rect {
        fill: rgba(200, 100, 100, 0.3);
        stroke: red;
    }

    .ct-label {
        font-size: 1.367rem;
    }
</style>
@endpush

@push('scripts')
    <script>
        const Transactions = {!! $transactions !!};
    </script>

<script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
<script src="{{ url('js/plugins/chartist/chartist-plugin-tooltip.min.js') }}"></script>
<script src="{{ url('js/plugins/chartist/chartist-plugin-legend.js') }}"></script>
<script src="{{ url('js/plugins/chartist/chartist-plugin-axistitle.min.js') }}"></script>
<script src="{{ url('js/plugins/chartist/chartist-plugin-zoom.min.js') }}"></script>
<script src="{{ url('js/plugins/chartist/chartist-plugin-pointlabels.min.js') }}"></script>



<script>
    jQuery(function($){
        var data = {
            // Our series array that contains series objects or in this case series data arrays
            series: [
                {
                    name: 'Income',
                    data: Transactions.filter(function(el) { return el.transaction_type_id == 1; }).map(function(el) {
                        return {
                            meta: el.short_description,
                            x: moment(el.date).valueOf(),
                            y: el.amount
                        };
                    }).sort(function(a, b) { return a.x < b.x ? -1 : a.x > b.x ? 1 : 0 })
                },
                {
                    name: 'Expenses',
                    data: Transactions.filter(function(el) { return el.transaction_type_id == 2; }).map(function(el) {
                        return {
                            meta: el.short_description,
                            x: moment(el.date).valueOf(),
                            y: el.amount
                        };
                    }).sort(function(a, b) { return a.x < b.x ? -1 : a.x > b.x ? 1 : 0 })
                }
            ],
        },
        settings = {
            plugins: [
                Chartist.plugins.tooltip({
                    transformTooltipTextFnc: function(amount) {
                        var parts = amount.split(','),
                            date = moment( parseInt(parts[0]) ).format("MMM DD, YYYY"),
                            time = moment( parseInt(parts[0]) ).format("hh:mm:ss a");



                        return date + '<br />' +
                                time + '<br />' +
                                '{{ env('CURRENCY_MARK', '$') }}' + parseFloat( parts[1] ).toFixed(2);
                    }
                }),
                Chartist.plugins.legend({
                    clickable: true
                }),
                Chartist.plugins.ctAxisTitle({
                    axisX: {
                        axisTitle: 'Date of Transaction',
                        axisClass: 'ct-axis-title',
                        offset: {
                            x: 0,
                            y: 32
                        },
                        textAnchor: 'middle'
                    },
                    axisY: {
                        axisTitle: 'Monetary Amount',
                        axisClass: 'ct-axis-title',
                        offset: {
                            x: -30,
                            y: 0
                        },
                        flipTitle: false
                    }
                }),
                Chartist.plugins.zoom({ onZoom: onZoom }),

                Chartist.plugins.ctPointLabels({
                    textAnchor: 'middle',
                    labelInterpolationFnc: function( amount ) {
                        var parts = amount.split(',');



                        return '{{ env('CURRENCY_MARK', '$') }}' + parseFloat( parts[1] ).toFixed(2);
                    }
                })
            ],

            axisX: {
                type: Chartist.AutoScaleAxis,

                // should be diff in days for date range
                divisor: Transactions.length,
                labelInterpolationFnc: function(value) {
                    return moment(value).format('MMM DD');
                }
            },

            lineSmooth: Chartist.Interpolation.simple({
                fillHoles: true,
            }),

            fullWidth: true
        };

        new Chartist.Line('.ct-chart', data, settings);

        var resetFnc;
        function onZoom(chart, reset) {
            resetFnc = reset;
        }

        var $btn = $("<button class='btn btn-default btn-lg'>Reset Zoom</button>").css({
            position: 'absolute',
            right: 0,
            bottom: 0,
            'z-index': 50,
            opacity: 0.7
        });

        $btn.on('click', function() {
            resetFnc && resetFnc();
        });

        $('.ct-chart').append($btn);

    });
</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js"></script>

        <!-- Morris Charts JavaScript -->
    <script src="{{ url('js/plugins/morris/raphael.min.js') }}"></script>
    <script src="{{ url('js/plugins/morris/morris.min.js') }}"></script>

    <!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><script src="{{ url('js/excanvas.min.js') }}"></script><![endif]-->
    <script src="{{ url('js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ url('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ url('js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ url('js/plugins/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ url('js/plugins/flot/jquery.flot.categories.js') }}"></script>
@endpush

@section('content')
    <div class="ct-chart ct-major-tenth"></div>
@endsection