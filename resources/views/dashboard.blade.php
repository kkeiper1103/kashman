@extends('layouts.application')

@section('title', "Dashboard")




@push('styles')
<link href="{{ url('css/plugins/morris.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ url('js/plugins/morris/raphael.min.js') }}"></script>
<script src="{{ url('js/plugins/morris/morris.min.js') }}"></script>

<!-- Flot Charts JavaScript -->
<!--[if lte IE 8]><script src="{{ url('js/excanvas.min.js') }}"></script><![endif]-->
<script src="{{ url('js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ url('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ url('js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ url('js/plugins/flot/jquery.flot.pie.js') }}"></script>
@endpush

@section("content")
    <div class="row">
        <div class="col-sm-2">
            @include('reports.charts.quick-stats')
        </div>
        <div class="col-sm-10">
            @include('reports.charts.gross')
        </div>
    </div>
@stop