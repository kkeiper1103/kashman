@extends('layouts.auth')

@section('title', "Login to your Account")

@section('content')


    <form method="POST" action="{{ url('auth/login') }}" class="form-horizontal">
        {!! csrf_field() !!}

        <div class="row">
            <div class="col-sm-10 col-sm-offset-2">
                <div class="page-header">
                    <h1 class="page-title">Login to Kashman</h1>
                </div>

                <p>Don't have an account? <a href="{{ url('auth/register') }}">Register!</a></p>
            </div>
        </div>



        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-8">
                <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Password</label>

            <div class="col-sm-8">
                <input type="password" name="password" id="password" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <label class="control-label">
                    <input type="checkbox" name="remember"> Remember Me
                </label>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </div>
    </form>
@stop
