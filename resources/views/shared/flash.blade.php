@if( session()->has('success') )
    <div class="alert alert-success" role="alert">
        <span>{{ session('success') }}</span>
    </div>
@endif


@if( session()->has('danger') )
    <div class="alert alert-danger" role="alert">
        <span>{{ session('danger') }}</span>
    </div>
@endif


<div id="alert-container">
    <template v-if="alerts.length">
        <div class="alert alert-@{{ a.type }}" v-for="a in alerts" role="alert">
            <i class="fa fa-times" @click='removeAlert($index)'></i>
            <span>@{{ a.message }}</span>
        </div>
    </template>
</div>


@push('scripts')
<script>
    jQuery(function($) {
        var alertVm = new Vue({
            el: "#alert-container",
            parent: window.bus,
            data: {
                alerts: []
            },
            events: {
                'update-alerts': function(alert) {
                    this.alerts.push(alert);
                }
            },
            methods: {
                removeAlert: function(index) {
                    this.alerts.splice(index, 1)
                }
            }
        });
    });
</script>
@endpush