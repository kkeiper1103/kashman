<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', 'Kashman')</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ url('css/sb-admin.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ url('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    
    @if( app()->environment() !== "production" )
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/0.7.13/vue-router.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.js"></script>
    @else
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-strap/1.0.11/vue-strap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/0.7.13/vue-router.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.9.3/vue-resource.min.js"></script>
    @endif




    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

    @stack('styles')

    <link rel="stylesheet" href="{{ url('css/styles.css') }}">
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">Kashman Financial Monitoring</a>
        </div>

        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-group"></i> {{ Auth::user()->group->name }} <b class="caret"></b>
                </a>

                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ url('settings/profile') }}"><i class="fa fa-fw fa-cog"></i> {{ Auth::user()->name }}</a>
                    </li>
                    @if( Auth::user()->admin )
                    <li>
                        <a href="{{ url('settings/group') }}"><i class="fa fa-group"></i> Group Settings</a>
                    </li>
                    @endif
                    <li class="divider"></li>
                    <li>
                        <a href="{{ url('auth/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="{{ url('/') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('transactions') }}"><i class="fa fa-fw fa-list-alt"></i> Transactions</a>
                </li>
                <li>
                    <a href="{{ url('reports') }}"><i class="fa fa-bar-chart"></i> Reports</a>
                </li>
                <li>
                    <a href="{{ url('settings/profile') }}"><i class="fa fa-fw fa-user"></i> User Profile</a>
                </li>
                @if( Auth::user()->admin )
                    <li>
                        <a href="{{ url('settings/group') }}"><i class="fa fa-group"></i> Group Settings</a>
                    </li>
                @endif
                <li>
                    <a href="{{ url('export') }}"><i class="fa fa-fw fa-exchange"></i> Export</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('title', 'PLEASE SET TITLE')</h1>

                    <ol class="breadcrumb">
                        @section('breadcrumbs')
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="{{ url('/') }}">Dashboard</a>
                        </li>
                        @show
                    </ol>
                </div>
            </div>
            <!-- /.row -->


            @include('shared.flash')


            @yield('content')


            <div class="footer-container text-center">
                <footer class="footer">
                    <span>Copyright &copy; {{ date('Y') }} Kyle Keiper &middot; All Rights Reserved. Software is provided as-is, and no warranty is given for the accuracy of this data.</span>
                </footer>
            </div>
        </div>
        <!-- /.container-fluid -->




    </div>
    <!-- /#page-wrapper -->

</div>

<!-- /#wrapper -->

<!-- jQuery -->
<script src="{{ url('js/jquery.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script>
    Vue.http.headers.common["X-CSRF-TOKEN"] = "{{ csrf_token() }}";

    var bus = new Vue();
</script>
<script>
    jQuery(function($) {
        $('input[type=datetime]').each(function() {
            $(this).parent().datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    });
</script>
@stack('scripts')

</body>

</html>
