# Kashman Finance Tracker

Kashman is a simple finance tracker for individuals. You can use this information to estimate cash flow, expenses, income, etc.

## Installation

1. Install [Composer](https://getcomposer.org).
2. Run `composer install` from the application folder.
3. Run `php artisan install` from the application folder.
4. Enjoy!

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## License

Kashman is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
