<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 6/28/16
 * Time: 2:12 PM
 */



if(!function_exists('str_humanize')) {
    /**
     * @param $string
     * @return mixed
     */
    function str_humanize($string) {

        return implode(' ', array_map('ucwords', explode('_', $string)));
    }
}

/**
 * @param mixed $args...
 * @return void
 */
function var_exit( $args = null ) {

    foreach(func_get_args() as $arg) {
        var_dump($arg);
    }

    exit;
}

/**
 * @param $obj
 * @return array
 */
function object2array( $obj )
{
    return (array) $obj;
}

/**
 * @param $arr
 * @return object
 */
function array2object( $arr )
{
    return (object) $arr;
}